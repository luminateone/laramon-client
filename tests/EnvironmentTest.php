<?php

namespace Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use LuminateOne\LaramonClient\Jobs\EnvironmentJob;

class EnvironmentTest extends TestCase
{
    public function testGetEnv() {
        $job = new EnvironmentJob();
        $env = $job->getEnv();

        //Is it getting an env file
        $this->assertNotEmpty($env);

        foreach ($env as $name => $value) {
            //Name of variable should never be empty
            $this->assertNotEmpty($name);

            //Both the name of the variable and the value should be being read as strings
            $this->assertInternalType('string', $name);
            $this->assertInternalType('string', $value);
        }
    }
}
