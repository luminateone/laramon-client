<?php

namespace Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use LuminateOne\LaramonClient\Jobs\ComposerDependenciesJob;

class ComposerTest extends TestCase
{
    /**
     * Read the composer dependencies, check it is in correct format
     * @return void
     */
    public function testReadDependencies()
    {
        $job = new ComposerDependenciesJob();
        $dependencies = $job->getComposerDependencies();

        //Check that it reads the dependencies
        $this->assertNotEmpty($dependencies);

        foreach ($dependencies as $package) {
            //Check that each package actually has a name and a version
            $this->assertNotEmpty($package->name);
            $this->assertNotEmpty($package->version);

            //Check these values are valid
            $this->assertInternalType('string', $package->name);
            $this->assertInternalType('string', $package->version);
        }
    }

    /**
     * Get the difference between the saved and new dependencies, check this difference is correct
     * @return void
     */
    public function testCompareDependencies() {
        //Make a saved package array
        $savedDependencies = [
            ["name" => "testZero", "version" => "0.1"],
            ["name" => "testOne", "version" => "0.1"],
            ["name" => "testTwo", "version" => "0.3"]
        ];
        //Make a new package array
        $newDependencies = [
            (object) ["name" => "testOne", "version" => "0.2"], //Different Version
            (object) ["name" => "testTwo", "version" => "0.3"], //The same
            (object) ["name" => "testThree", "version" => "0.1"] //New completely
        ];

        $job = new ComposerDependenciesJob();
        $difDependencies = $job->compareDependencies($savedDependencies, $newDependencies);

        //Only two packages are different, check this is correct
        $this->assertCount(2, $difDependencies);
        //Check it has updated the version number of the dependency
        $this->assertTrue($difDependencies[0]['version'] === "0.2");
        //Check it has added the new dependency
        $this->assertTrue($difDependencies[1]['name'] === "testThree");
    }

}
