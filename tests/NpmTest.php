<?php

namespace Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use LuminateOne\LaramonClient\Jobs\NpmDependenciesJob;

class NpmTest extends TestCase
{
    /**
     * Read the npm dependencies, check it is in correct format
     * @return void
     */
    public function testReadDependencies()
    {
        $job = new NpmDependenciesJob();
        $dependencies = $job->getNpmDependencies();

        //Check that it reads the dependencies
        $this->assertNotEmpty($dependencies);

        foreach ($dependencies as $name => $package) {
            //Check that each package actually has a name and a version
            $this->assertNotEmpty($name);
            $this->assertNotEmpty($package->version);

            //Check these values are valid
            $this->assertInternalType('string', $name);
            $this->assertInternalType('string', $package->version);
        }
    }

    /**
     * Get the difference between the saved and new dependencies, check this difference is correct
     * @return void
     */
    public function testCompareDependencies() {
        //Make a saved package array
        $savedDependencies = [
            ["name" => "testZero", "version" => "0.1", "integrity" => "zero"],
            ["name" => "testOne", "version" => "0.1", "integrity" => "one"],
            ["name" => "testTwo", "version" => "0.3", "integrity" => "two"]
        ];
        // Make a new dependencies object, imitating the package-lock file
        //  - Test One has a different version and integrity
        //  - Test Two is the same
        //  - Test Three is new completely
        $newDependencies = json_decode('{
            "dependencies": {
                "testOne": {
                    "version": "0.2",
                    "integrity": "two"
                },
                "testTwo": {
                    "version": "0.3",
                    "integrity": "two"
                },
                "testThree": {
                    "version": "0.1",
                    "integrity": "three"
                }
            }
        }');

        $job = new NpmDependenciesJob();
        $difDependencies = $job->compareDependencies($savedDependencies, $newDependencies->dependencies);

        //Only two packages are different, check this is correct
        $this->assertCount(2, $difDependencies);
        //Check it has updated the version number of the dependency
        $this->assertTrue($difDependencies[0]['version'] === "0.2");
        //Check it has added the new dependency
        $this->assertTrue($difDependencies[1]['name'] === "testThree");
        //Check the integrity has a warning for different integrity
        $this->assertTrue($difDependencies[0]['integrity_error']);
        //Check the valid integrity has no warning
        $this->assertTrue($difDependencies[1]['integrity_error'] == null);
    }
}
