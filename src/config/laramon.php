<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Laramon Access Code
    |--------------------------------------------------------------------------
    |
    | This is the access code that is provided when site is set up.
    |
    */
    'access_code' => env('LARAMON_ACCESS_CODE', null)
];