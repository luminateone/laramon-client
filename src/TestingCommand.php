<?php namespace LuminateOne\LaramonClient;

use LuminateOne\LaramonClient\Jobs\ComposerDependenciesJob;
use LuminateOne\LaramonClient\Jobs\NpmDependenciesJob;
use LuminateOne\LaramonClient\Jobs\EnvironmentJob;
use Illuminate\Console\Command;

class TestingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'laramon:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Finds all composer dependencies and sends new packages to the Laramon Website';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Installation successful!";
//        ComposerDependenciesJob::dispatch();
//        NpmDependenciesJob::dispatch();
//        EnvironmentJob::dispatch();
    }

}
