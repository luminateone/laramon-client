<?php namespace LuminateOne\LaramonClient;

use Illuminate\Support\ServiceProvider;
use LuminateOne\LaramonClient\TestingCommand;

class LaramonServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/laramon.php' => config_path('laramon.php')
        ]);
        $this->loadRoutesFrom(__DIR__.'/routes.php');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
           TestingCommand::class
        ]);
    }
}
