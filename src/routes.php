<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use LuminateOne\LaramonClient\Classes\GetComposerDependenciesClass;
use LuminateOne\LaramonClient\Classes\GetNpmDependenciesClass;

Route::prefix('laramon')->group(function () {
    // Composer Packages
    Route::get('heartbeat', function () {
        return "alive";
    });

    // Return detailed information about composer / npm / env
    Route::get('info', function (Request $request) {
        // Check they have a token
        if (!isset($request->header()['authorization']))
            return "Unauthorized";

        // Check the token is valid
        if (config('laramon.access_code') !== $request->header()['authorization'][0])
            return "Unauthorized";

        $info = [];

        // Composer Packages
        $composerPackages = new GetComposerDependenciesClass();
        $info['composer'] = $composerPackages->execute();

        // NPM Packages
        $npmPackages = new GetNpmDependenciesClass();
        $info['npm'] = $npmPackages->execute();

        // ENV Information
        $dbName = config('database.default');
        $dbConnections = config('database.connections');

        $info['env'] = [];
        $info['env']['APP_ENV'] = config('app.env');
        $info['env']['DB_DATABASE'] = is_array($dbName[$dbConnections]) ? $dbName[$dbConnections]['database'] : null;

        // PHP version
        $info['php_version'] = phpversion();

        return $info;
    });
});
