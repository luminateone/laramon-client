<?php namespace LuminateOne\LaramonClient\Classes;

class GetNpmDependenciesClass
{

    private $savedDependencies = [];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return array
     */
    public function execute()
    {
        //Get the npm dependencies from the package.lock file
        $dependencies = $this->getNpmDependencies();
        //Compare saved dependencies to new dependencies
        $newDependencies = $this->compareDependencies($this->savedDependencies, $dependencies);
        //Sent them to laramon website
        return $newDependencies;
    }

    /**
     * Get all of the NPM installed dependencies in json format
     * @return array
     */
    public function getNpmDependencies() {
        //Open the file in read only
        $file = fopen('../package-lock.json','r');
        //Read and the json file
        $json = fread($file, filesize('../package-lock.json'));
        //Convert json into php object and get the dependencies
        $dependencies = json_decode($json)->dependencies;
        //Close the file
        fclose($file);

        return $dependencies;
    }

    /**
     * Return an array of npm dependencies that are different to what is currently saved
     * @return array
     */
    public function compareDependencies($savedDependencies, $dependencies) {

        $newDependencies = [];

        //Look at each package in the new json file
        foreach ($dependencies as $name => $package) {

            $found = false;
            $update = false;
            $integrityAlert = false;

            //Check that the name doesn't exist in the saved dependencies
            foreach ($savedDependencies as $savedPackage) {
                if ($savedPackage['name'] === $name) {

                    //Check the version of package isn't different
                    if ($savedPackage['version'] !== $package->version) {
                        //Update the saved package to new package version
                        $savedPackage['version'] = $package->version;
                        $update = true;
                    }

                    //Check the integrity of package isn't different
                    if ($savedPackage['integrity'] !== $package->integrity) {
                        //Update the saved package to new package integrity?
                        $savedPackage['integrity'] = $package->integrity;
                        $update = true;
                        $integrityAlert = true;
                    }

                    //If the version or the integrity changed, add it to new array
                    if ($update) {
                        $newPackage['name'] = $name;
                        $newPackage['version'] = $package->version;
                        if ($integrityAlert) {
                            $newPackage['integrity_error'] = true;
                        }
                        $newPackage['integrity'] = $package->integrity;

                        array_push($newDependencies, $newPackage);
                    }

                    $found = true;
                    break;
                }
            }

            //If the name is not in the saved dependencies, add it
            if (!$found) {
                $newPackage['name'] = $name;
                $newPackage['version'] = $package->version;
                $newPackage['integrity'] = $package->integrity;
                $newPackage['integrity_error'] = null;

                array_push($newDependencies, $newPackage);
            }
        }

        //Return all of the new packages
        return $newDependencies;
    }
}
