<?php namespace LuminateOne\LaramonClient\Classes;

class GetComposerDependenciesClass
{

    private $savedComposerDependencies = [];
    private $savedLockDependencies = [];
    private $savedLockDependenciesDependencies = [];

    /**
     * Create a new class instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return array
     */
    public function execute()
    {
        //Get the different dependencies from the composer files
        $composerDependencies = $this->getComposerDependencies();
        $lockDependencies = $this->getLockDependencies();
        $lockDependenciesDependencies = $this->getLockDependenciesDependencies();
        //Compare saved dependencies to new dependencies
        $newComposerDependencies = $this->compareDependencies2($this->savedComposerDependencies, $composerDependencies);
        $newLockDependencies = $this->compareDependencies($this->savedLockDependencies, $lockDependencies);
        $newLockDependenciesDependencies = $this->compareDependencies3($this->savedLockDependenciesDependencies, $lockDependenciesDependencies);
        //Sent them to laramon website
        return $this->sendDependencies($newComposerDependencies, $newLockDependencies, $newLockDependenciesDependencies);
    }

    /**
     * Get all of the composer installed dependencies in json format
     * @return array
     */
    public function getComposerDependencies() {
        //Open the file in read onlyC:\Users\jeremy\Desktop\projects\laramon-client\composer.json
        $file = fopen('../composer.json','r');
        //Read and decode the json file
        $dependencies = json_decode(fread($file, filesize('../composer.json')));
        //Close the file
        fclose($file);
        $encode = json_encode($dependencies->require);
        $encode = json_decode($encode, true);
        //Return all of the packages
        return $encode;
    }

    /**
     * Get all of the main lock installed dependencies in json format
     * @return array
     */
    public function getLockDependencies() {
        //Open the file in read only
        $file = fopen('../composer.lock','r');
        //Read and decode the json file
        $dependencies = json_decode(fread($file, filesize('../composer.lock')));
        //Close the file
        fclose($file);
        //Return all of the packages
        return $dependencies->packages;
    }

    /**
     * Get all of the dependencies dependencies located inside the lock file in json format
     * @return array
     */
    public function getLockDependenciesDependencies() {
        //Open the file in read only
        $file = fopen('../composer.lock','r');
        //Read and decode the json file
        $dependencies = json_decode(fread($file, filesize('../composer.lock')));
        //Close the file
        fclose($file);
        //Find each of the dependencies dependencies
        $dependenciesDependencies = [];
        foreach ($dependencies->packages as $package) {
            if (isset($package->require)) {
                foreach ($package->require as $pName => $pValue) {
                    //if it is actually a package
                    if (strpos($pName, '/') !== false) {
                        //save it
                        array_push($dependenciesDependencies, ["name" => $pName, "version" => $pValue]);
                    }
                }
            }
        }
        //Return all of the packages
        return $dependenciesDependencies;
    }

    /**
     * Compare saved dependencies to read dependencies
     *
     * @param $savedDependencies
     * @param $dependencies
     * @return array
     */
    public function compareDependencies($savedDependencies, $dependencies) {

        $newDependencies = [];

        usort($savedDependencies, function($a, $b) {
            return $a['name'] <=> $b['name'];
        });

        usort($dependencies, function($a, $b) {
            return $a->name <=> $b->name;
        });

        //Look at each package in the new json file
        foreach ($dependencies as $package) {

            $found = false;

            //Check that the name doesn't exist in the saved dependencies
            foreach ($savedDependencies as $savedPackage) {
                if ($savedPackage['name'] === $package->name) {

                    //Check the version of package isn't different
                    if ($savedPackage['version'] !== $package->version) {
                        //Update the saved package to new package version
                        $savedPackage['version'] = $package->version;

                        $newPackage['name'] = $package->name;
                        $newPackage['version'] = $package->version;

                        array_push($newDependencies, $newPackage);
                    }
                    $found = true;
                    break;
                }
            }

            //If the name is not in the saved dependencies, add it
            if (!$found) {
                $newPackage['name'] = $package->name;
                $newPackage['version'] = $package->version;

                array_push($newDependencies, $newPackage);
            }

        }

        //Return all of the new packages
        return $newDependencies;
    }

    /**
     * Compare saved dependencies to read dependencies
     *
     * @param $savedDependencies
     * @param $dependencies
     * @return array
     */
    public function compareDependencies2($savedDependencies, $dependencies) {

        $newDependencies = [];

        //Look at each package in the new json file
        foreach ($dependencies as $pName => $pVersion) {

            $found = false;

            //Check that the name doesn't exist in the saved dependencies
            foreach ($savedDependencies as $savedPackage) {
                if ($savedPackage['name'] === $pName) {

                    //Check the version of package isn't different
                    if ($savedPackage['version'] !== $pVersion) {
                        //Update the saved package to new package version
                        $savedPackage['version'] = $pVersion;

                        $newPackage['name'] = $pName;
                        $newPackage['version'] = $pVersion;

                        array_push($newDependencies, $newPackage);
                    }
                    $found = true;
                    break;
                }
            }

            //If the name is not in the saved dependencies, add it
            if (!$found) {
                $newPackage['name'] = $pName;
                $newPackage['version'] = $pVersion;

                array_push($newDependencies, $newPackage);
            }
        }

        //Return all of the new packages
        return $newDependencies;
    }

    /**
     * Compare saved dependencies to read dependencies
     *
     * @param $savedDependencies
     * @param $dependencies
     * @return array
     */
    public function compareDependencies3($savedDependencies, $dependencies) {

        $newDependencies = [];

        //Look at each package in the new json file
        foreach ($dependencies as $package) {

            $found = false;

            //Check that the name doesn't exist in the saved dependencies
            foreach ($savedDependencies as $savedPackage) {
                if ($savedPackage['name'] === $package['name']) {

                    //Check the version of package isn't different
                    if ($savedPackage['version'] !== $package['version']) {
                        //Update the saved package to new package version
                        $savedPackage['version'] = $package['version'];

                        $newPackage['name'] = $package['name'];
                        $newPackage['version'] = $package['version'];

                        array_push($newDependencies, $newPackage);
                    }
                    $found = true;
                    break;
                }
            }

            //If the name is not in the saved dependencies, add it
            if (!$found) {
                $newPackage['name'] = $package['name'];
                $newPackage['version'] = $package['version'];

                array_push($newDependencies, $newPackage);
            }
        }

        //Return all of the new packages
        return $newDependencies;
    }

    /**
     * Send new dependencies to Laramon website
     * @param $composer
     * @param $lock
     * @param $lockDepend
     *
     * @return array
     */
    private function sendDependencies($composer, $lock, $lockDepend) {
        $sendArray = ["composer" => $composer, "lock" => $lock, "lockDepend" => $lockDepend];
        return $sendArray;
    }
}
